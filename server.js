const serverless = require('serverless-http');
const bodyParser = require('body-parser');
const cors = require('cors');
const express = require('express')
const app = express()
const AWS = require('aws-sdk');
const AmazonCognitoIdentity = require('amazon-cognito-identity-js');
const dynamoDb = new AWS.DynamoDB.DocumentClient();
const CognitoExpress = require('cognito-express');

// imorting lib for generating randon string
const cryptoRandomString = require('crypto-random-string');

// amazon cognito creds
var USER_POOL_ID = 'us-east-1_16iaX17ke'
var CLIENT_ID = '5ol0jfemh918kq53gg2q7auuf6'

// cors and bodyparser initialisation
app.use(cors());
app.use(bodyParser.json());

// dynamo db tables
var serverTable = "cloud-eye-dev-server";
var groupTable = "cloud-eye-dev-group";


const cognitoExpress = new CognitoExpress({
    region: "us-east-1",
    cognitoUserPoolId: USER_POOL_ID,
    tokenUse: "id", //Possible Values: access | id
    tokenExpiration: 3600000 //Up to default expiration of 1 hour (3600000 ms)
});

//authentication route
app.use(function (req, res, next) {
    let idTokenFromClient = req.headers['id_token'];
    if (!idTokenFromClient) {
        return res.status(401).json({ message: "id_token not found" })
    }
    cognitoExpress.validate(idTokenFromClient, function (err, response) {
        if (err) {
            console.log("Middleware authenticate Error", err);
            return res.status(401).json({ message: err })
        } else {
            req.user = response; //Optional - if you want to capture user information
            next();
        }
    });
})

/* Api to add the server and make relation between server and group
Method - POST
*/
app.post('/api/server', async function (req, res) {
    var body = req.body;
    body.creationDate = new Date().getTime();
    body.serverId = cryptoRandomString({ length: 6, type: 'alphanumeric' });
    // body.lastpingOn = new Date(body.lastpingOn).getTime();
    // get login user details with access_token
    var userDetails = req.user;
    //console.log("user", userDetails);
    body.createdBy = userDetails.email;
    console.log("body",body);
    //check if workspace already exists
    const serverExists = await dynamoDb.get({
        TableName: serverTable,
        Key: {
            serverId: body.serverId
        }
    }).promise();
    if (serverExists && serverExists.Item) {
        return res.status(400).json({ message: "Server already Exists" });
    } else {
        var params = {
            TableName: serverTable,
            Item: body
        };
        const server = await dynamoDb.put(params).promise();
        //console.log("workspace", workspace);
        if (!server) {
            return res.status(400).json({ message: 'Unable to add server ' + err });
        } else {
            // relation for workspace and user
            var obj = {
                groupId: cryptoRandomString({ length: 6, type: 'alphanumeric' }),
                createdBy: userDetails.email,  // dynamic
                createdOn: new Date().getTime()
            }
            var params = {
                TableName: groupTable,
                Item: obj
            };
            const group = await dynamoDb.put(params).promise();
            //console.log("group", group);
            res.status(201).json({ message: "Success" });

        }
    }
})

// function to get the server based on the login user
app.get('/api/server', async function (req, res) {
    var userDetails = req.user;
    const server = await dynamoDb.query({
        TableName: serverTable,
        KeyConditionExpression: 'createdBy=:createdBy',
        ExpressionAttributeValues: {
            ':createdBy': userDetails.email
        },
    }).promise();
    if (!server.Items) {
        return res.status(404).json({ message: `Servers  not found` })
    } else {
        res.json({ message: "Success", data: server });
    }
})

app.get('/api/server/:id', async function (req, res) {
    const server = await dynamoDb.get({
        TableName: serverTable,
        Key: {
            serverId: req.params.id
        }
    }).promise();
    if (!server && !server.Item) {
        return res.status(400).json({ message: "Server not found" });
    } else {
        res.json({ message: "Success", data: server.Item });
    }
})

app.put('/api/server/:id', async function(req, res) {
    var body = req.body;
    const serverId = req.params.id;
    var params = {
        TableName: serverTable,
        Key: {
            serverId: serverId
        },
        UpdateExpression: "set lastpingOn = :p",
        ExpressionAttributeValues: {
            ":p": body.lastpingOn
        },
        ReturnValues: "UPDATED_NEW"
    };

    console.log("Updating the item...");
    await dynamoDb.update(params).promise();
    return res.status(202).json({ message: "Success" });
})

app.delete('/api/server/:id', async function(req, res) {
    const serverId = req.params.id;
    var params = {
        TableName: serverTable,
        Key: {
            serverId: serverId
        }
    };

    console.log("Attempting a conditional delete...");
    await dynamoDb.delete(params).promise();
    // get groupId from the group table
    var groupDetails = await getServerGroup(serverId)
    if (groupDetails.message != "Group not found") {
        var params1 = {
            TableName: groupTable,
            Key: {
                groupId: groupDetails.Group.groupId
            }
        };
        await dynamoDb.delete(params1).promise();
        res.json({ message: "Success" })
    } else {
        return res.status(400).json({ message: "Fail!" })
    }

})

async function getServerGroup(serverId) {
    const group = await dynamoDb.get({
        TableName: groupTable,
        Key: {
            serverId: serverId
        }
    }).promise();
    if (!group && !group.Item) {
        return { message: "Group not found" };
    } else {
        return { message: "Success", Group: group.Item };
    }
}



module.exports.handler = serverless(app);