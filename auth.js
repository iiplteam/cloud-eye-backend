const serverless = require('serverless-http');
const bodyParser = require('body-parser');
const cors = require('cors');
const express = require('express')
const app = express()
const AWS = require('aws-sdk');
const AmazonCognitoIdentity = require('amazon-cognito-identity-js');
var USER_POOL_ID = 'us-east-1_16iaX17ke'
var CLIENT_ID = '5ol0jfemh918kq53gg2q7auuf6'
app.use(cors());
app.use(bodyParser.json())

app.post('/api/auth/create', function (req, res) {
    var body = req.body;
    var poolData = {
        UserPoolId: USER_POOL_ID, // Your user pool id here
        ClientId: CLIENT_ID, // Your client id here
    };
    var userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);

    var attributeList = [];

    var dataEmail = {
        Name: 'email',
        Value: body.email
    };

    var dataName = {
        Name: 'name',
        Value: body.name,
    };
    var attributeEmail = new AmazonCognitoIdentity.CognitoUserAttribute(dataEmail);
    var attributeName = new AmazonCognitoIdentity.CognitoUserAttribute(
        dataName
    );

    attributeList.push(attributeEmail);
    attributeList.push(attributeName);

    userPool.signUp(body.email, body.password, attributeList, null, function (
        err,
        result
    ) {
        if (err) {
            console.log(err.message || JSON.stringify(err));
            res.status(400).json({ message: err.message })
            return;
        }
        var cognitoUser = result.user;
        console.log('user name is ' + cognitoUser.getUsername());
        res.json({ message: "Successfully registered. Please check your mail for verification code!" });
    });
});

app.post('/api/auth/verify-code', function (req, res) {
    var body = req.body;
    var poolData = {
        UserPoolId: USER_POOL_ID, // Your user pool id here
        ClientId: CLIENT_ID, // Your client id here
    };
    var userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);
    var userData = {
        Username: body.email,
        Pool: userPool,
    };
    var cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);
    cognitoUser.confirmRegistration(body.verificationCode, true, function (err, result) {
        if (err) {
            console.log(err.message || JSON.stringify(err));
            return res.status(401).json({ message: err.message })
        }
        console.log('call result: ' + result);
        res.json({ message: "Verification Success" })
    });
})

app.post('/api/auth/login', function (req, res) {
    var body = req.body;
    var authenticationData = {
        Username: body.email,
        Password: body.password,
    };
    var authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails(
        authenticationData
    );
    var poolData = {
        UserPoolId: USER_POOL_ID, // Your user pool id here
        ClientId: CLIENT_ID, // Your client id here
    };
    var userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);
    var userData = {
        Username: body.email,
        Pool: userPool,
    };
    var cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);
    cognitoUser.authenticateUser(authenticationDetails, {
        onSuccess: function (result) {
            console.log("result", result);
            var accessToken = result.accessToken.jwtToken;
            var idToken = result.idToken.jwtToken;
            var refreshToken = result.refreshToken.token;
            //POTENTIAL: Region needs to be set if not already set previously elsewhere.
            // AWS.config.region = '';

            // AWS.config.credentials = new AWS.CognitoIdentityCredentials({
            //     IdentityPoolId: '...', // your identity pool id here
            //     Logins: {
            //         // Change the key below according to the specific region your user pool is in.
            //         'cognito-idp.<region>.amazonaws.com/<YOUR_USER_POOL_ID>': result
            //             .getIdToken()
            //             .getJwtToken(),
            //     },
            // });

            //refreshes credentials using AWS.CognitoIdentity.getCredentialsForIdentity()
            AWS.config.credentials.refresh(error => {
                if (error) {
                    console.error(error);
                } else {
                    // Instantiate aws sdk service objects now that the credentials have been updated.
                    // example: var s3 = new AWS.S3();
                    console.log('Successfully logged!');
                    res.send({
                        message: "Successfully logged!",
                        access_token: accessToken,
                        id_token: idToken,
                        refresh_token: refreshToken
                    })
                }
            });
        },

        onFailure: function (err) {
            console.log(err.message || JSON.stringify(err));
            res.status(403).send({ message: err.message })
        },
    });
})

app.post('/api/auth/forgot-password', function (req, res) {
    var body = req.body;
    var poolData = {
        UserPoolId: USER_POOL_ID, // Your user pool id here
        ClientId: CLIENT_ID, // Your client id here
    };
    var userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);
    var userData = {
        Username: body.email,
        Pool: userPool,
    };
    var cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);
    cognitoUser.forgotPassword({
        onSuccess: function (data) {
            // successfully initiated reset password request
            console.log('CodeDeliveryData from forgotPassword: ' + data);
            res.json({ message: "Verification code sent to " + body.email, data: data })
        },
        onFailure: function (err) {
            console.log(err.message || JSON.stringify(err));
            return res.status(400).json({ message: err.message });
        },

    });
})

app.post('/api/auth/confirm-forgot-password', function (req, res) {
    var body = req.body;
    var poolData = {
        UserPoolId: USER_POOL_ID, // Your user pool id here
        ClientId: CLIENT_ID, // Your client id here
    };
    var userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);
    var userData = {
        Username: body.email,
        Pool: userPool,
    };
    var cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);
    //Optional automatic callback
    cognitoUser.confirmPassword(body.verificationCode, body.newPassword, {
        onSuccess() {
            console.log('Password confirmed!');
            res.json({message:"Password Changed Successfully!"});
        },
        onFailure(err) {
            console.log('Password not confirmed!');
            return res.status(400).json({ message: err.message })
        },
    });
})

app.post('/api/auth/reset-password', function (req, res) {
    var body = req.body;
    var access_token = req.headers['access_token'];
    var id_token = req.headers['id_token'];
    var refresh_token = req.headers['refresh_token'];
    var poolData = {
        UserPoolId: USER_POOL_ID, // Your user pool id here
        ClientId: CLIENT_ID, // Your client id here
        access_token: access_token,
        id_token: id_token,
        refresh_token: refresh_token
    };
    var userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);
    var userData = {
        Username: body.email,
        Pool: userPool,
    };
    var cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);
    cognitoUser.changePassword(body.oldPassword, body.newPassword, function (err, result) {
        if (err) {
            console.log(err.message || JSON.stringify(err));
            res.status(400).json({ message: err.message })
            return;
        }
        console.log('call result: ' + result);
        res.json({ message: "Reset Password Success", data: result })
    });
})



module.exports.handler = serverless(app);