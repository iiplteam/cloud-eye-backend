const serverless = require('serverless-http');
const bodyParser = require('body-parser');
const cors = require('cors');
const express = require('express')
const app = express()
const AWS = require('aws-sdk');
const AmazonCognitoIdentity = require('amazon-cognito-identity-js');
const dynamoDb = new AWS.DynamoDB.DocumentClient();
const CognitoExpress = require('cognito-express');

// imorting lib for generating randon string
const cryptoRandomString = require('crypto-random-string');

// amazon cognito creds
var USER_POOL_ID = 'us-east-1_16iaX17ke'
var CLIENT_ID = '5ol0jfemh918kq53gg2q7auuf6'

// cors and bodyparser initialisation
app.use(cors());
app.use(bodyParser.json());

// dynamo db tables
var workspaceTable = "cloud-eye-dev-workspace";
var workspaceUsersTable = "cloud-eye-dev-workspace-users";

const cognitoExpress = new CognitoExpress({
    region: "us-east-1",
    cognitoUserPoolId: USER_POOL_ID,
    tokenUse: "id", //Possible Values: access | id
    tokenExpiration: 3600000 //Up to default expiration of 1 hour (3600000 ms)
});

//authentication route
app.use(function (req, res, next) {
    let idTokenFromClient = req.headers['id_token'];
    if(!idTokenFromClient) {
        return res.status(401).json({message: "id_token not found"})
    }
    cognitoExpress.validate(idTokenFromClient, function (err, response) {
        if (err) {
            console.log("Middleware authenticate Error",err);
            return res.status(401).json({ message: err})
        } else {
            req.user = response; //Optional - if you want to capture user information
            next();
        }
    });
})

/* Api to add the workspace and make relation between workspace and user
Method - POST
*/
app.post('/api/workspace', async function (req, res) {
    var body = req.body;
    body.creationDate = new Date().getTime();
    body.appId = cryptoRandomString({length: 2, type: 'distinguishable'})+cryptoRandomString({length: 3, type: 'numeric'})+cryptoRandomString({length: 3, type: 'distinguishable'});
    body.appSecret = cryptoRandomString({length: 24, type: 'alphanumeric'});

    // get login user details with access_token
    var userDetails = req.user;
    //console.log("user", userDetails);
    body.createdBy = userDetails.email;

    //check if workspace already exists
    const workspaceExists = await dynamoDb.get({
        TableName: workspaceTable,
        Key: {
            workspaceId: body.workspaceId
        }
    }).promise();
    if (workspaceExists && workspaceExists.Item) {
        return res.status(400).json({ message: "Workspace already Exists" });
    } else {
        var params = {
            TableName: workspaceTable,
            Item: body
        };
        const workspace = await dynamoDb.put(params).promise();
        //console.log("workspace", workspace);
        if (!workspace) {
            return res.status(400).json({ message: 'Unable to add workspace ' + err });
        } else {
            // relation for workspace and user
            var obj = {
                workspaceId: body.workspaceId,
                userId: userDetails.email  // dynamic
            }
            var params = {
                TableName: workspaceUsersTable,
                Item: obj
            };
            const workspace2users = await dynamoDb.put(params).promise();
            //console.log("workspace2users", workspace2users);
            res.status(201).json({ message: "Success" });

        }
    }
})

// function to get the workspaces based on the login user
app.get('/api/workspace', async function (req, res) {
    var userDetails = req.user;
    const workspaces = await dynamoDb.query({
        TableName: workspaceTable,
        KeyConditionExpression: 'createdBy=:createdBy',
        ExpressionAttributeValues: {
           ':createdBy': userDetails.email
        },
    }).promise();
    if (!workspaces.Items) {
        return res.status(404).json({ message: `Worksapces  not found` })
    } else {
        res.json({ message: "Success", data: workspaces });
    }
})

app.get('/api/workspace/:id', async function getWorkspaceById(req, res) {
    const workspace = await dynamoDb.get({
        TableName: workspaceTable,
        Key: {
            workspaceId: req.params.id
        }
    }).promise();
    if (!workspace && !workspace.Item) {
        return res.status(400).json({ message: "workspace not found" });
    } else {
        res.json({ message: "Success", data: workspace.Item });
    }
})

app.put('/api/workspace/:id', async function updateWorkspace(req, res) {
    var body = req.body;
    const workspaceID = req.params.id;
    var params = {
        TableName:workspaceTable,
        Key:{
            workspaceId: workspaceID
        },
        UpdateExpression: "set email = :e , wname = :n , phone = :p, website = :w",
        ExpressionAttributeValues:{
            ":n":body.wname,
            ":e":body.email,
            ":p":body.phone,
            ":w":body.website
        },
        ReturnValues:"UPDATED_NEW"
    };
    
    //console.log("Updating the item...");
     await dynamoDb.update(params).promise();
    return res.status(202).json({ message: "Success" });
})

app.delete('/api/workspace/:id', async function deleteWorkspace(req, res) {
    const workspaceID = req.params.id;
    var params = {
        TableName: workspaceTable,
        Key: {
            workspaceId: workspaceID
        }
    };

    //console.log("Attempting a conditional delete...");
    await dynamoDb.delete(params).promise();
    var params1 = {
        TableName: workspaceUsersTable,
        Key: {
            workspaceId: workspaceID
        }
    };
    await dynamoDb.delete(params1).promise();
    res.json({ message: "Success" })
})



module.exports.handler = serverless(app);